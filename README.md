# Bomberman Serveur Web

<div align="center">
<img width="300" height="400" src="web_server.jpg">
</div>

## Description du projet

Application web réalisée avec Java EE et SE en MASTER INFO 1 à l'université d'Angers dans le cadre des modules "Web avancé" et "Programmation réseaux" durant l'année 2019-2020. \
Elle se décompose en deux parties :
- Un site Web (boutique) JEE qui impactent l'expérience de jeu Bomberman (nouvelles cartes, cosmétiques, etc...) et activation de certaines améliorations en fonction du nombre de victoire/succès ;
- Un serveur de jeu qui s'éxecute sur le même système d'information que le JEE.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr ;
- Mohamed OUHIRRA : mouhirra@etud.univ-angers.fr ;
- Anas TAGUENITI : atagueniti@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'unversité d'Angers :
- Benoît DA MOTA : benoit.da-mota@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre des modules "Web avancé" et "Programmation réseaux" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2020 sur la période du confinement COVID-19 à la maison au mois de Mars-Avril. \
Il a été terminé et rendu le 09/04/2020.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Maven ;
- Servlet ;
- JSP ;
- E.L. ;
- Tomcat ;
- Sockets TCP ;
- Thread.

## Objectif

Permettre de démarrer les environnements JEE et réseau Bomberman.

### Partie JEE

JEE répondant aux contraintes suivantes :
- Création/modification/suppression/consultation des comptes ;
- Consultation de l'historique des parties ;
- Classement journalier/mensuel/etc. des joueurs par nombre de victoires, ratio victoires/défaites, etc.

### Partie réseaux 

Client et serveur de jeu Bomberman répondant aux contraintes suivantes :
- Identification des utilisateurs avec les mêmes identifiants que pour la partie JEE ;
- Le serveur enregistre les informations des parties jouées (date, scores, défaite/victoire, etc.) pour exploitation avec JEE ;
- Le serveur gére plusieurs clients simultanément ;
- Conception anti-triche : c'est le serveur qui décide/valide ce qui se passe dans une partie ;
- Protocole interopérable et humainement lisible (JSON).

## Installation JEE

- Lancer la ligne de commande suivante pour démarrer le serveur Tomcat :
./Projet/TomcatServer/bin/startup.sh

- Pour arréter le serveur Tomcat :
./Projet/TomcatServer/bin/shutdown.sh

- Taper l'URL dans un navigateur web 
http://localhost:8080/project-web-jee-bomberman/

## Installation Réseau

- Lancer d'abord le JEE (voir section Installation JEE) pour accéder à l'API

- Puis lancer le serveur avec la commande suivante :
java -jar Projet/ProjectServerBomberman/target/project-server-bomberman-0.0.1-SNAPSHOT-jar-with-dependencies.jar

- Enfin lancer le jeu avec la commande suivante :
java -jar Projet/ProjectClientBomberman/target/project-client-bomberman-0.0.1-SNAPSHOT-jar-with-dependencies.jar