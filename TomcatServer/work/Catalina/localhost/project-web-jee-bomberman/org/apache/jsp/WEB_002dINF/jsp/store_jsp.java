/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/9.0.30
 * Generated at: 2020-04-08 23:20:31 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class store_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(3);
    _jspx_dependants.put("jar:file:/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Avance/TP/workspace/TomcatServer/webapps/project-web-jee-bomberman/WEB-INF/lib/jstl-1.2.jar!/META-INF/fmt.tld", Long.valueOf(1153377882000L));
    _jspx_dependants.put("/WEB-INF/lib/jstl-1.2.jar", Long.valueOf(1575994770000L));
    _jspx_dependants.put("jar:file:/media/etudiant/2DCF15A2510DBD36/Cours/Master/M1/Web_Avance/TP/workspace/TomcatServer/webapps/project-web-jee-bomberman/WEB-INF/lib/jstl-1.2.jar!/META-INF/c.tld", Long.valueOf(1153377882000L));
  }

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fif_0026_005ftest;

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fc_005fif_0026_005ftest = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.release();
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
      throws java.io.IOException, javax.servlet.ServletException {

    if (!javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
      final java.lang.String _jspx_method = request.getMethod();
      if ("OPTIONS".equals(_jspx_method)) {
        response.setHeader("Allow","GET, HEAD, POST, OPTIONS");
        return;
      }
      if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method)) {
        response.setHeader("Allow","GET, HEAD, POST, OPTIONS");
        response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Les JSPs ne permettent que GET, POST ou HEAD. Jasper permet aussi OPTIONS");
        return;
      }
    }

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("    \n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\" >\n");
      out.write("<head>\n");
      out.write("  <meta charset=\"UTF-8\">\n");
      out.write("  <title>Bomberman - Compte</title>\n");
      out.write("  <link href=\"imports/Bomberman-icon.png\" rel=\"icon\" sizes=\"128x128\">\n");
      out.write("     \n");
      out.write("   <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/myStyle.css\">   \n");
      out.write("   \n");
      out.write("   \n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/reset.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/site.css\">\n");
      out.write("\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/container.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/grid.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/header.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/image.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/menu.css\">\n");
      out.write("\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/divider.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/segment.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/form.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/input.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/button.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/list.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/message.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/myStyle.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"Semantic/components/icon.css\">\n");
      out.write("\n");
      out.write("  <script src=\"Semantic/components/jquery.min.js\"></script>\n");
      out.write("  <script src=\"Semantic/components/form.js\"></script>\n");
      out.write("  <script src=\"Semantic/components/transition.js\"></script>\n");
      out.write("  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.4/semantic.css'>\n");
      out.write("  <link rel=\"stylesheet\" href=\"Semantic/components/stylehome.css\">\n");
      out.write("  \n");
      out.write("  <link href=\"//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("<link href=\"https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("<script src=\"https://code.jquery.com/jquery-2.1.4.js\"></script>\n");
      out.write("<script src=\"https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.js\"></script>\n");
      out.write("\t  \n");
      out.write("<script>\n");
      out.write("      $(function(){\n");
      out.write("          $(\"#logo\").load(\"imports/logo.html\");           \n");
      out.write("      });\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("<div class=\"ui sidebar vertical left menu overlay visible\" style=\"-webkit-transition-duration: 0.1s; overflow: visible !important;\">\n");
      out.write("\t<div class=\"item logo\">    \n");
      out.write("\t  <div id=\"logo\"></div>\n");
      out.write("\t</div>\n");
      out.write("\t\n");
      out.write("\t<div class=\"ui accordion\">\n");
      out.write("\t\t<a class=\"item\" href=\"http://localhost:8080/project-web-jee-bomberman/StoreServlet\">\n");
      out.write("\t\t\t<b>Ma boutique</b>\n");
      out.write("\t\t</a>\n");
      out.write("\t\n");
      out.write("\t\t<a class=\"item\" href=\"http://localhost:8080/project-web-jee-bomberman/DownloadServlet\">\n");
      out.write("\t\t\t<b>Télécharger le jeu</b>\n");
      out.write("\t\t</a>\n");
      out.write("\t\t\n");
      out.write("\t\t<a class=\"item\" href=\"http://localhost:8080/project-web-jee-bomberman/UpdateAccountServlet\">\n");
      out.write("\t\t\t<b>Modifier mon compte</b>\n");
      out.write("\t\t</a>\t\n");
      out.write("\t\t\n");
      out.write("\t\t<a class=\"item\" href=\"http://localhost:8080/project-web-jee-bomberman/AccountServlet\">\n");
      out.write("\t\t\t<b>Mon compte</b>\n");
      out.write("\t\t</a>\t\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t<a class=\"item\" href=\"http://localhost:8080/project-web-jee-bomberman/GameServlet\">\n");
      out.write("\t\t\t<b>Historique des parties</b>\n");
      out.write("\t\t</a>\t\n");
      out.write("\t\t\n");
      out.write("\t\t<a class=\"item\" href=\"http://localhost:8080/project-web-jee-bomberman/RankingServlet\">\n");
      out.write("\t\t\t<b>Classement</b>\n");
      out.write("\t\t</a>\t\n");
      out.write("\t    \n");
      out.write("\t\t<a class=\"item\" href=\"http://localhost:8080/project-web-jee-bomberman/DeleteAccountServlet\">\n");
      out.write("\t\t\t<b>Supprimer mon compte</b>\n");
      out.write("\t\t</a>\n");
      out.write("\t</div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<div class=\"pusher\">\n");
      out.write("  <div class=\"ui menu asd borderless\" style=\"border-radius: 0!important; border: 0; margin-left: 260px; -webkit-transition-duration: 0.1s;\">   \n");
      out.write("    ");
      if (_jspx_meth_c_005fif_005f0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    \n");
      out.write("    <div class=\"right menu\">      \n");
      out.write("      \t<div class=\"item\">                        \n");
      out.write("        \t<button class=\"ui icon negative button\" type=\"submit\" id=\"action-delete\">\n");
      out.write("        \t\tDéconnexion\t\t  \t\t\n");
      out.write("\t\t\t</button>\n");
      out.write("        </div>\n");
      out.write("    </div>    \n");
      out.write("  </div>\n");
      out.write("  \n");
      out.write("  \t<div class=\"content_sidebar\">\n");
      out.write("\t  \t<h1>Les items disponibles</h1>  \n");
      out.write("\t\t\n");
      out.write("\t\t    <form class=\"ui mini modal\" method=\"get\" action=\"DeconnexionServlet\">\t\n");
      out.write("\t\t\t  <div class=\"header\">Déconnexion</div>\n");
      out.write("\t\t\t  <div class=\"actions\">\n");
      out.write("\t\t\t    <div class=\"ui deny button\">\n");
      out.write("\t\t\t      Annuler\n");
      out.write("\t\t\t    </div>\t\n");
      out.write("\t\t\t    <button class=\"ui negative right labeled icon button\" type=\"submit\" id=\"delete\">\n");
      out.write("\t\t\t      Confirmer\n");
      out.write("\t\t\t      <i class=\"trash alternate icon\"></i>\n");
      out.write("\t\t\t    </button>\n");
      out.write("\t\t\t  </div>\n");
      out.write("\t\t\t</form>\n");
      out.write("\t\t\n");
      out.write("\t\t\n");
      out.write("\t\t\n");
      out.write("\t\t<div class=\"ui centered grid\">\n");
      out.write("\t\t  <div class=\"computer only row\">\n");
      out.write("\t\t    <div class=\"column\"></div>\n");
      out.write("\t\t  </div>\n");
      out.write("\t\t  <div class=\"six wide tablet eight wide computer column\">\n");
      out.write("\t\t  \t<div class=\"ui styled accordion\">\n");
      out.write("\t\t\t  ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${store.display()}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\t\t  \n");
      out.write("\t\t\t</div>\t\t\t\n");
      out.write("\t\t  </div>\n");
      out.write("\t\t  \n");
      out.write("\t\t  <div class=\"six wide tablet eight wide computer column\">\n");
      out.write("\t\t  \t");
      if (_jspx_meth_c_005fif_005f1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\t\t  </div>\n");
      out.write("\t\t</div>\n");
      out.write("\t\t\n");
      out.write("\t      \t\t      \t\n");
      out.write("\t</div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("  <script>\n");
      out.write("\t$(function(){\n");
      out.write("\t\t$(\"#action-delete\").click(function(){\n");
      out.write("\t\t\t$('.mini.modal')\n");
      out.write("\t     \t.modal('setting', 'closable', false)\n");
      out.write("\t     \t.modal('show');\n");
      out.write("\t\t});\n");
      out.write("\t});\n");
      out.write("\t</script>\n");
      out.write("\t\n");
      out.write("\t<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js'></script>\n");
      out.write("\t<script src='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.4/semantic.js'></script>\n");
      out.write("\t<script src=\"Semantic/components/scripthome.js\"></script>\n");
      out.write("\t\n");
      out.write("\t</body>\n");
      out.write("</html>\n");
      out.write("  \t  ");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_005fif_005f0(javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    boolean _jspx_th_c_005fif_005f0_reused = false;
    try {
      _jspx_th_c_005fif_005f0.setPageContext(_jspx_page_context);
      _jspx_th_c_005fif_005f0.setParent(null);
      // /WEB-INF/jsp/store.jsp(92,4) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fif_005f0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${!empty sessionScope.sessionUtilisateur}", boolean.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null)).booleanValue());
      int _jspx_eval_c_005fif_005f0 = _jspx_th_c_005fif_005f0.doStartTag();
      if (_jspx_eval_c_005fif_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("                \n");
          out.write("        <h3 class=\"pseudo_title\" style=\"padding-top: 9px; padding-left: 11px;\">Bienvenue dans le magasin Bomberman</h3>        \n");
          out.write("    ");
          int evalDoAfterBody = _jspx_th_c_005fif_005f0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_005fif_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
      _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f0);
      _jspx_th_c_005fif_005f0_reused = true;
    } finally {
      org.apache.jasper.runtime.JspRuntimeLibrary.releaseTag(_jspx_th_c_005fif_005f0, _jsp_getInstanceManager(), _jspx_th_c_005fif_005f0_reused);
    }
    return false;
  }

  private boolean _jspx_meth_c_005fif_005f1(javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f1 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    boolean _jspx_th_c_005fif_005f1_reused = false;
    try {
      _jspx_th_c_005fif_005f1.setPageContext(_jspx_page_context);
      _jspx_th_c_005fif_005f1.setParent(null);
      // /WEB-INF/jsp/store.jsp(134,5) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fif_005f1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${not empty item}", boolean.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null)).booleanValue());
      int _jspx_eval_c_005fif_005f1 = _jspx_th_c_005fif_005f1.doStartTag();
      if (_jspx_eval_c_005fif_005f1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("\t\t\t   <div class=\"ui items\">\n");
          out.write("\t\t\t  <div class=\"item\">\n");
          out.write("\t\t\t    <div class=\"ui small image\">\n");
          out.write("\t\t\t      <img src=\"public/images/");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${item.pathPicture}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
          out.write("\">\n");
          out.write("\t\t\t    </div>\n");
          out.write("\t\t\t    <div class=\"content\">\n");
          out.write("\t\t\t      <div class=\"header\"><h1>Item ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${item.name}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
          out.write("</h1></div>\n");
          out.write("\t\t\t      <div class=\"meta\">\n");
          out.write("\t\t\t        <span class=\"price\">Prix : ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${item.price}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
          out.write(" €</span>\t        \n");
          out.write("\t\t\t      </div>\n");
          out.write("\t\t\t      <div class=\"description\">\n");
          out.write("\t\t\t        <p><strong>Condition :</strong> ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${item.condition}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
          out.write("</p>\n");
          out.write("\t\t\t        <p style=\"width:80%\"><strong>Description :</strong> ");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${item.description}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
          out.write("</p>\n");
          out.write("\t\t\t      </div>\n");
          out.write("\t\t\t      \n");
          out.write("\t\t\t     ");
          if (_jspx_meth_c_005fif_005f2(_jspx_th_c_005fif_005f1, _jspx_page_context))
            return true;
          out.write("\n");
          out.write("\t\t\t      \n");
          out.write("\t\t\t    </div>\n");
          out.write("\t\t\t  </div>\t  \t  \n");
          out.write("\t\t\t</div>   \n");
          out.write("\t\t\t");
          int evalDoAfterBody = _jspx_th_c_005fif_005f1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_005fif_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
      _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f1);
      _jspx_th_c_005fif_005f1_reused = true;
    } finally {
      org.apache.jasper.runtime.JspRuntimeLibrary.releaseTag(_jspx_th_c_005fif_005f1, _jsp_getInstanceManager(), _jspx_th_c_005fif_005f1_reused);
    }
    return false;
  }

  private boolean _jspx_meth_c_005fif_005f2(javax.servlet.jsp.tagext.JspTag _jspx_th_c_005fif_005f1, javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_005fif_005f2 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    boolean _jspx_th_c_005fif_005f2_reused = false;
    try {
      _jspx_th_c_005fif_005f2.setPageContext(_jspx_page_context);
      _jspx_th_c_005fif_005f2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_005fif_005f1);
      // /WEB-INF/jsp/store.jsp(150,8) name = test type = boolean reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fif_005f2.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${isPossibleToAccess}", boolean.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null)).booleanValue());
      int _jspx_eval_c_005fif_005f2 = _jspx_th_c_005fif_005f2.doStartTag();
      if (_jspx_eval_c_005fif_005f2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("\t\t\t\t      <div style=\"width:80%\" class=\"extra\">\n");
          out.write("\t\t\t              <form method=\"post\" action=\"ItemServlet\">\n");
          out.write("\t\t\t              \t<input type=\"hidden\" id=\"itemIdentifiant\" name=\"itemIdentifiant\" value=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${item.identifiant}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
          out.write("\">\n");
          out.write("\t\t\t\t\t        <input type=\"submit\" value=\"Débloquer\" class=\"ui right floated primary button\" />\n");
          out.write("\t\t\t\t\t      </form>\t\t        \n");
          out.write("\t\t\t\t      </div>\n");
          out.write("\t\t\t     ");
          int evalDoAfterBody = _jspx_th_c_005fif_005f2.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_005fif_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
      _005fjspx_005ftagPool_005fc_005fif_0026_005ftest.reuse(_jspx_th_c_005fif_005f2);
      _jspx_th_c_005fif_005f2_reused = true;
    } finally {
      org.apache.jasper.runtime.JspRuntimeLibrary.releaseTag(_jspx_th_c_005fif_005f2, _jsp_getInstanceManager(), _jspx_th_c_005fif_005f2_reused);
    }
    return false;
  }
}
