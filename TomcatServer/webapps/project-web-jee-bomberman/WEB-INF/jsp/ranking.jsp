<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>    
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Bomberman - Classement</title>
   <link href="imports/Bomberman-icon.png" rel="icon" sizes="128x128">
   
   
  <link rel="stylesheet" type="text/css" href="Semantic/components/reset.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/site.css">

  <link rel="stylesheet" type="text/css" href="Semantic/components/container.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/grid.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/header.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/image.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/menu.css">

  <link rel="stylesheet" type="text/css" href="Semantic/components/divider.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/segment.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/form.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/input.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/button.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/list.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/message.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/myStyle.css">
  <link rel="stylesheet" type="text/css" href="Semantic/components/icon.css">
  
  <script src="Semantic/components/form.js"></script>
  <script src="Semantic/components/transition.js"></script>
  
  
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.4/semantic.css'>
  <link rel="stylesheet" href="Semantic/components/stylehome.css">
  
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.css" rel="stylesheet" type="text/css" />
<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script src="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.js"></script>
   <link rel="stylesheet" type="text/css" href="Semantic/components/myStyle.css">
   
   <script>
       $(function(){
           $("#logo").load("imports/logo.html");
           $("table").tablesort();
       });             
   </script>
</head>

<body>

<div class="ui sidebar vertical left menu overlay visible" style="-webkit-transition-duration: 0.1s; overflow: visible !important;">
	<div class="item logo">
	   <div id="logo"></div>
	</div>
	<div class="ui accordion">
		<a class="item" href="http://localhost:8080/project-web-jee-bomberman/StoreServlet">
			<b>Ma boutique</b>
		</a>
	
		<a class="item" href="http://localhost:8080/project-web-jee-bomberman/DownloadServlet">
			<b>Télécharger le jeu</b>
		</a>
		
		<a class="item" href="http://localhost:8080/project-web-jee-bomberman/UpdateAccountServlet">
			<b>Modifier mon compte</b>
		</a>	
		
		<a class="item" href="http://localhost:8080/project-web-jee-bomberman/AccountServlet">
			<b>Mon compte</b>
		</a>	
				
		<a class="item" href="http://localhost:8080/project-web-jee-bomberman/GameServlet">
			<b>Historique des parties</b>
		</a>	
		
		<a class="item" href="http://localhost:8080/project-web-jee-bomberman/RankingServlet">
			<b>Classement</b>
		</a>
	    
		<a class="item" href="http://localhost:8080/project-web-jee-bomberman/DeleteAccountServlet">
			<b>Supprimer mon compte</b>
		</a>
	</div>
	
</div>

<div class="pusher">
  <div class="ui menu asd borderless" style="border-radius: 0!important; border: 0; margin-left: 260px; -webkit-transition-duration: 0.1s;">

    <c:if test="${!empty sessionScope.sessionUtilisateur}">                
        <h3 class="pseudo_title" style="padding-top: 9px; padding-left: 11px;">Bienvenue <c:out value="${sessionScope.sessionUtilisateur.pseudo }"/></h3>        
    </c:if>
    
    <div class="right menu">      
      	<div class="item">                        
        	<button class="ui icon negative button" type="submit" id="action-delete">
        		Déconnexion		  		
			</button>
        </div>
    </div>   
    
  </div>
  
  <div class="content_sidebar">
  <h1>Classement</h1>
  
    		<form class="ui mini modal" method="get" action="DeconnexionServlet">	
			  <div class="header">Déconnexion</div>
			  <div class="actions">
			    <div class="ui deny button">
			      Annuler
			    </div>	
			    <button class="ui negative right labeled icon button" type="submit" id="delete">
			      Confirmer
			      <i class="trash alternate icon"></i>
			    </button>
			  </div>
			</form>
  
  <table class="ui sortable celled table">
  	  <caption style="font-size:25px;text-align:left;">Par score victoire</caption>
	  <thead>
	    <tr>
	    	<th>Pays</th>
	    	<th>Pseudo</th>
	    	<th>Rang</th>
	  		<th class="sorted ascending">Score</th>
	  	</tr>
	  </thead>
	  <tbody>
	  
	  <c:forEach var="account" items="${Accounts}">
	  	<c:set var="score" value="0" scope="page"/>
	    <tr>
	      <td data-label="Pays"><c:out value="${account.user.country}"/></td>
	      <td data-label="Pseudo"><c:out value="${account.user.pseudo}"/></td>
	      <td data-label="Rang"><c:out value="${account.user.rank.name}"/></td>
	      
	      <c:forEach var="gameWon" items="${account.gamesWon }">
	      	<c:set var="score" value="${score+gameWon.score }" scope="page"/>
	      </c:forEach>
	      
	      <td data-label="Score"><c:out value="${score }"/></td>
	      	    
	    </tr>
	  </c:forEach>  	  	    	    	    
	  </tbody>
	</table>
	
	<br/>
	<br/>
	
	  <table class="ui sortable celled table">
	  <caption style="font-size:25px;text-align:left;">Ratio victoire/défaite</caption>
	  <thead>
	    <tr>
	    	<th>Pays</th>
	    	<th>Pseudo</th>
	    	<th>Rang</th>
	  		<th class="sorted ascending">Score</th>
	  	</tr>
	  </thead>
	  <tbody>		  
	  
	  <c:forEach var="account" items="${Accounts}">	  	
	    <tr>
	      <td data-label="Pays"><c:out value="${account.user.country}"/></td>
	      <td data-label="Pseudo"><c:out value="${account.user.pseudo}"/></td>
	      <td data-label="Rang"><c:out value="${account.user.rank.name}"/></td>	      
	      <td data-label="Score"><c:out value="${fn:length(account.gamesWon) / fn:length(account.gamesLost) }"/></td>
	    </tr>
	  </c:forEach>  	  	    	    	    
	  </tbody>
	</table>
  </div>
  </div>

  <script>
	$(function(){
		$("#action-delete").click(function(){
			$('.mini.modal')
	     	.modal('setting', 'closable', false)
	     	.modal('show');
		});
	});
	</script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.4/semantic.js'></script>
<script src="Semantic/components/scripthome.js"></script>
<script src="Semantic/components/tablesort.js"></script>
</body>
</html>
  	  